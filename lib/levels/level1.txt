map_data=[['g','g','g','g','g'],
          ['g','s','s','s','g'],
          ['g','s','m','s','g'],
          ['g','s','s','s','g'],
          ['g','g','g','g','g']]
		
player_pos=[0,0]
player_gold=250

plot_cost=100

win_num_plants=3

plants={'sunflower':['sunflower.png',30,0.2,200,65],
        'pumpkin':['pumpkin.png',100,0.035,100,210],
	'violet':['violet.png',50,0.15,100,110],
	'fourLeafClover':['four-leaf-clover.png',25,0.25,100,50],
	'carrots':['carrots.png',75,0.05,100,110] }

nourish_cost=25#how much it costs you to 'nourish' a plant
nourish_increase=1.5#how much faster the plant grows, you can only nourish once

locust_cost=25
locust_consume_speed=200#how high the timer is, the higher the number, the slower the locust swarm

lightning_blast_cost=55

stun_duration=300

enemy_start_gold=200

enemy_pos=[6,6]

enemy_aggresiveness=10

enemy_speed=1
enemy_cost_multiplier=1.5

next_level="level2.txt"

level_start_image=None
