from modules import core, isometric, entities, gui, elements, hud, level_loader
from modules.core import *
from modules.elements import *
from modules.hud import *

import time

try:
    import psyco
    psyco.background()
except:
    print "now psyco installed here..."

class Engine(object):
    def __init__(self):
        self.state="menu"
        self.last_level=level_loader.load_config(os.path.join('data','config.txt'))
        self.start_level="level1.txt"

        self.won_gold=0

        pygame.init()
        try:
            self.screen=pygame.display.set_mode([640,480], FULLSCREEN)
        except:
            self.screen=pygame.display.set_mode([640,480])

    def main(self):
        while 1:
            if self.state=='QUIT':
                a=open(os.path.join('data','config.txt'),'wb')
                a.write('last_level="%s"'%self.last_level)
                a.close()
                pygame.quit()
                return

            if self.state=="YOU GOLD LOST":
                core.Sound(os.path.join('data','sfx','you_lose.wav')).play()
                self.fadeout()
                self.YouGoldLost()
                self.fadeout()
                continue

            if self.state=='menu':
                pygame.mixer.music.load(os.path.join('data','sfx','pianayoga.ogg'))
                pygame.mixer.music.play(-1)
                self.fadeout()
                self.mainmenu()
                self.fadeout()
                continue

            if self.state=="game":
                pygame.mixer.music.load(os.path.join('data','sfx','Nanhara.ogg'))
                pygame.mixer.music.play(-1)
                a=self.game(self.last_level)
                continue

            if self.state=="tut1":
                self.fadeout()
                self.tut1()
                self.fadeout()

            if self.state=="tut2":
                self.tut2()
                self.fadeout()

            if self.state=="tut3":
                self.tut3()
                self.fadeout()

            if self.state=="tut4":
                self.tut4()
                self.fadeout()

            if self.state=="tut5":
                self.tut5()
                self.fadeout()

            if self.state=="tut6":
                self.tut6()
                self.fadeout()

            if self.state=="tut7":
                self.tut7()
                self.fadeout()

            if self.state=="Startgame":
                pygame.mixer.music.load(os.path.join('data','sfx','Nanhara.ogg'))
                pygame.mixer.music.play(-1)
                a=self.game(self.start_level)

            if self.state=="YOU WON":
                self.fadeout()
                self.YouWon()
                self.fadeout()
                continue

            if self.state=="YOU WON THE GAME":
                self.fadeout()
                self.YouWonTheGame()
                self.fadeout()
                continue

            if self.state=="YOU LOST":
                core.Sound(os.path.join('data','sfx','you_lose.wav')).play()
                self.fadeout()
                self.YouLost()
                self.fadeout()
                continue

    def tut1(self):
        image=core.load_image(os.path.join('data','tutorial',
                                           '1.bmp'))
        mouse=GenericMouse()
        while 1:
            for event in pygame.event.get():
                if event.type==MOUSEBUTTONDOWN or\
                   event.type==KEYDOWN or\
                   event.type==QUIT:
                    self.state="tut2"
                    return
            self.screen.blit(image,(0,0))
            mouse.render(self.screen)
            pygame.display.flip()

    def tut2(self):
        image=core.load_image(os.path.join('data','tutorial',
                                           '2.bmp'))
        mouse=GenericMouse()
        while 1:
            for event in pygame.event.get():
                if event.type==MOUSEBUTTONDOWN or\
                   event.type==KEYDOWN or\
                   event.type==QUIT:
                    self.state="tut3"
                    return
            self.screen.blit(image,(0,0))
            mouse.render(self.screen)
            pygame.display.flip()

    def tut3(self):
        image=core.load_image(os.path.join('data','tutorial',
                                           '3.bmp'))
        mouse=GenericMouse()
        while 1:
            for event in pygame.event.get():
                if event.type==MOUSEBUTTONDOWN or\
                   event.type==KEYDOWN or\
                   event.type==QUIT:
                    self.state="tut4"
                    return
            self.screen.blit(image,(0,0))
            mouse.render(self.screen)
            pygame.display.flip()

    def tut4(self):
        image=core.load_image(os.path.join('data','tutorial',
                                           '4.bmp'))
        mouse=GenericMouse()
        while 1:
            for event in pygame.event.get():
                if event.type==MOUSEBUTTONDOWN or\
                   event.type==KEYDOWN or\
                   event.type==QUIT:
                    self.state="tut5"
                    return
            self.screen.blit(image,(0,0))
            mouse.render(self.screen)
            pygame.display.flip()

    def tut5(self):
        image=core.load_image(os.path.join('data','tutorial',
                                           '5.bmp'))
        mouse=GenericMouse()
        while 1:
            for event in pygame.event.get():
                if event.type==MOUSEBUTTONDOWN or\
                   event.type==KEYDOWN or\
                   event.type==QUIT:
                    self.state="tut6"
                    return
            self.screen.blit(image,(0,0))
            mouse.render(self.screen)
            pygame.display.flip()

    def tut6(self):
        image=core.load_image(os.path.join('data','tutorial',
                                           '6.bmp'))
        mouse=GenericMouse()
        while 1:
            for event in pygame.event.get():
                if event.type==MOUSEBUTTONDOWN or\
                   event.type==KEYDOWN or\
                   event.type==QUIT:
                    self.state="tut7"
                    return
            self.screen.blit(image,(0,0))
            mouse.render(self.screen)
            pygame.display.flip()

    def tut7(self):
        image=core.load_image(os.path.join('data','tutorial',
                                           '7.bmp'))
        mouse=GenericMouse()
        while 1:
            for event in pygame.event.get():
                if event.type==MOUSEBUTTONDOWN or\
                   event.type==KEYDOWN or\
                   event.type==QUIT:
                    self.state="menu"
                    return
            self.screen.blit(image,(0,0))
            mouse.render(self.screen)
            pygame.display.flip()

    def mainmenu(self):
        self.screen.fill([255,255,255])
        def nofunc():
            return

        title=core.load_image(os.path.join('data','menu','title.bmp'))
        start_main_game=gui.Button([100,350], core.load_image(os.path.join('data',
                                                                           'menu',
                                                                           'flower.bmp')),
                                   'Begin Regular Game',nofunc,
                                   os.path.join('data','assets','Onciale.ttf'),18,
                                   font_color=[0,0,0,255],
                                   font_hover_color=[255,0,0,255],
                                   click_button=1)
        start_tutorial=gui.Button([435,351], core.load_image(os.path.join('data',
                                                                           'menu',
                                                                           'scythe.bmp')),
                                   'Tutorial',nofunc,
                                   os.path.join('data','assets','Onciale.ttf'),18,
                                   font_color=[0,0,0,255],
                                   font_hover_color=[255,0,0,255],
                                  click_button=1)

        continue_button=gui.Button([280,350], core.load_image(os.path.join('data',
                                                                           'menu',
                                                                           'water_can.bmp')),
                                   'ContinueGame',nofunc,
                                   os.path.join('data','assets','Onciale.ttf'),18,
                                   font_color=[0,0,0,255],
                                   font_hover_color=[255,0,0,255],
                                  click_button=1)
        quit_button=gui.Button([550,375],core.load_image(os.path.join('data',
                                                                           'menu',
                                                                           'gold.png'),
                                                         alpha=True),
                                'Quit',nofunc,
                                os.path.join('data','assets','Onciale.ttf'),32,
                                font_color=[0,0,255,255],
                                font_hover_color=[255,0,0,255],
                                click_button=1)
        clock=pygame.time.Clock()

        mouse=GenericMouse()
        while 1:
            clock.tick(99)
            for event in pygame.event.get():

                start_main_game.update(event)
                start_tutorial.update(event)
                quit_button.update(event)
                continue_button.update(event)
                if event.type==QUIT:
                    self.state="QUIT"
                    return

            self.screen.fill([255,255,255,255])
            start_main_game.render(self.screen)
            start_tutorial.render(self.screen)
            quit_button.render(self.screen)
            continue_button.render(self.screen)
            self.screen.blit(title, (0,0))
            mouse.render(self.screen)
            if quit_button.am_clicked:
                self.state="QUIT"
                return
            if start_main_game.am_clicked:
                self.state='Startgame'
                return
            if continue_button.am_clicked:
                self.state='game'
                return
            if start_tutorial.am_clicked:
                self.state='tut1'
                return
            pygame.display.flip()

    def YouWon(self):
        image=core.load_image(os.path.join('data','assets','scroll.png'),
                                   alpha=True)
        font=pygame.font.Font(os.path.join('data','assets','Onciale.ttf'),90)
        fi=font.render('You Won', True, [0,175,0,255])
        rf=fi.get_rect()
        rf.center=(320,240)


        ri=image.get_rect()
        ri.center=(320,240)
        clock=pygame.time.Clock()

        mouse=GenericMouse()
        while 1:
            clock.tick(99)
            for event in pygame.event.get():
                if event.type==QUIT:
                    self.state="menu"
                    return
                if event.type==MOUSEBUTTONDOWN or\
                   event.type==KEYDOWN:
                    self.state='game'
                    return

            self.screen.fill([255,255,255])
            self.screen.blit(image, ri)
            self.screen.blit(fi, rf)
            mouse.render(self.screen)
            pygame.display.flip()

    def YouWonTheGame(self):
        image=core.load_image(os.path.join('data','assets','scroll.png'),
                                   alpha=True)
        font=pygame.font.Font(os.path.join('data','assets','Onciale.ttf'),90)
        font2=pygame.font.Font(os.path.join('data','assets','Onciale.ttf'),30)
        fi=font.render('You Won', True, [0,175,0,255])
        rf=fi.get_rect()
        rf.center=(320,240)

        lines=[]
        lines.append(font2.render("Congradulations! you won the whole tournament!", True, [0,175,0,255]))
        lines.append(font2.render("This deserves a reward or something...", True, [0,175,0,255]))
        lines.append(font2.render("I know...", True, [0,175,0,255]))
        lines.append(font2.render("you can have the fruits of your labor...", True, [0,175,0,255]))
        lines.append(font2.render("%s Gold pieces!"%self.won_gold, True, [0,175,0,255]))
        rects=[]
        down=0
        for i in lines:
            r=i.get_rect()
            r.center=(320,25+down)
            rects.append(r)
            down+=33

        ri=image.get_rect()
        ri.center=(320,240)
        clock=pygame.time.Clock()

        mouse=GenericMouse()
        while 1:
            clock.tick(99)
            for event in pygame.event.get():
                if event.type==QUIT:
                    self.state="menu"
                    return
                if event.type==MOUSEBUTTONDOWN or\
                   event.type==KEYDOWN:
                    self.state='menu'
                    return

            self.screen.fill([255,255,255])
            self.screen.blit(image, ri)
            self.screen.blit(fi, rf)

            for i in range(len(lines)):
                self.screen.blit(lines[i],rects[i])
            mouse.render(self.screen)
            pygame.display.flip()

    def YouLost(self):
        image=core.load_image(os.path.join('data','assets','scroll.png'),
                                   alpha=True)
        font=pygame.font.Font(os.path.join('data','assets','Onciale.ttf'),90)
        fi=font.render('You Lost', True, [255,125,0,255])
        rf=fi.get_rect()
        rf.center=(320,240)


        ri=image.get_rect()
        ri.center=(320,240)
        clock=pygame.time.Clock()

        mouse=GenericMouse()
        while 1:
            clock.tick(99)
            for event in pygame.event.get():
                if event.type==QUIT:
                    self.state="menu"
                    return
                if event.type==MOUSEBUTTONDOWN or\
                   event.type==KEYDOWN:
                    self.state='menu'
                    return

            self.screen.fill([255,255,255])
            self.screen.blit(image, ri)
            self.screen.blit(fi, rf)
            mouse.render(self.screen)
            pygame.display.flip()

    def YouGoldLost(self):
        image=core.load_image(os.path.join('data','assets','scroll.png'),
                                   alpha=True)
        font=pygame.font.Font(os.path.join('data','assets','Onciale.ttf'),90)
        font2=pygame.font.Font(os.path.join('data','assets','Onciale.ttf'),35)
        fi=font.render('You Lost', True, [255,125,0,255])
        rf=fi.get_rect()
        rf.center=(320,240)

        gold=font2.render("You ran out of gold!", True, [255,0,0,255])
        gr=gold.get_rect()
        gr.center=(320,125)

        ri=image.get_rect()
        ri.center=(320,240)
        clock=pygame.time.Clock()

        mouse=GenericMouse()
        while 1:
            clock.tick(99)
            for event in pygame.event.get():
                if event.type==QUIT:
                    self.state="menu"
                    return
                if event.type==MOUSEBUTTONDOWN or\
                   event.type==KEYDOWN:
                    self.state='menu'
                    return

            self.screen.fill([255,255,255])
            self.screen.blit(image, ri)
            self.screen.blit(fi, rf)
            self.screen.blit(gold, gr)
            mouse.render(self.screen)
            pygame.display.flip()

    def show_level_image(self):
        l=level_loader.load_dict(os.path.join('levels',self.last_level))
        if l['level_start_image']:
            image=core.load_image(os.path.join('levels','data',l['level_start_image']))
        else:
            image=pygame.Surface((640,480))
            image.fill([255,255,255])
            f=pygame.font.Font(os.path.join('data','assets','Onciale.ttf'),110)
            f=f.render('READY?', True, [0,0,255])
            r=f.get_rect()
            r.center=(320,240)
            image.blit(f, r)

        clock=pygame.time.Clock()

        mouse=GenericMouse()

        while 1:
            clock.tick(99)
            for event in pygame.event.get():
                if event.type==QUIT:
                    self.state="menu"
                    return
                if event.type==MOUSEBUTTONDOWN or\
                   event.type==KEYDOWN:
                    self.state='game'
                    return

            self.screen.blit(image, (0,0))
            mouse.render(self.screen)
            pygame.display.flip()

    def fadeout(self, color=[255,255,255]):
        image=pygame.Surface((640,480))
        r=image.get_rect()
        r.topleft=(0,0)
        pygame.draw.rect(image, color, r)
        image.set_alpha(35)
        for i in range(15):
            time.sleep(0.075)
            self.screen.blit(image, r)
            pygame.display.flip()
        return

    def fadein(self, image):
        image.set_alpha(15)
        for i in range(18):
            time.sleep(0.075)
            self.screen.blit(image, (0,0))
            pygame.display.flip()
                    

    def game(self, level):
        level=level_loader.load_dict(os.path.join('levels',level))
        self.show_level_image()
        self.screen.fill([0,0,0])

        for i in level['plants']:
            level['plants'][i][0]=core.load_image(os.path.join('data','objects',
                                                                level['plants'][i][0]),
                                                       alpha=True)

        world=isometric.IsoMap(level['map'],
                               {'g':core.load_image(os.path.join('data','tiles','grass.bmp'),-1),
                                's':core.load_image(os.path.join('data','tiles','stone.png'),-1),
                                'm':core.load_image(os.path.join('data','tiles','moss.png'),-1)},
                               100,
                               core.load_image(os.path.join('data','tiles','tile_mask.bmp')))

        player=Player(world, level)
        enemy=Enemy(world, level)

        game_win_plants=level['win_num_plants']

    ##    pygame.mixer.music.load(os.path.join('data','assets','testing.ogg'))
    ##    pygame.mixer.music.play(-1)

        hud=HUD(player)
        hovering=Hovering_on()
        mouse=Mouse()

        world.move(-400,-100)
        clock=pygame.time.Clock()
        pygame.key.set_repeat(9)

        player.update(world, enemy)
        enemy.update(world, player)

        fimage=pygame.Surface((640,480))
        world.render(fimage)
        player.render(fimage)
        enemy.render(fimage)
        a=self.fadein(fimage)
        if self.state=="menu"or self.state=="QUIT":
            return
        del fimage

        while 1:
            clock.tick(90)
            for event in pygame.event.get():
                hud.event(event)
                if event.type==KEYDOWN:
                    if event.key==K_ESCAPE:
                        self.state='menu'
                        return

                    if event.key==K_s:
                        pygame.image.save(screen, 'screenie.bmp')

                if event.type==QUIT:
                    self.state='menu'
                    return

                if event.type==MOUSEBUTTONDOWN:
                    if event.button==1:
                        if player.stunned:
                            continue
                        pos=world.get_tile_at_pos(pygame.mouse.get_pos())
                        if pos==None:
                            continue
                        if not hud.fixed_size.collidepoint(pygame.mouse.get_pos()):
                            if hud.mouse_mode=='buy_plot':
                                if not pos in [enemy.entity.pos]+enemy.plots.STs:
                                    player.buy_plot(pos, world)
                            if hud.mouse_mode=='buy_plant':
                                if not pos in [enemy.entity.pos]+enemy.plots.STs:
                                    player.buy_plant(pos, world, hud.selected_plant)
                            if hud.mouse_mode=='harvest':
                                player.harvest(pos)
                            if hud.mouse_mode=='nourish':
                                player.nourish(pos, world)
                            if hud.mouse_mode=='lightning':
                                player.cast_lightning_bolt(pos, world, enemy)
                            if hud.mouse_mode=="locust":
                                player.cast_locusts(pos, world)

                    if event.button==3:
                        hud.mouse_mode='select'
                        hud.plant_list_on=False

            mp = pygame.mouse.get_pos()
            if mp[0]==0:
                world.move(-2,0)
            if mp[0]==639:
                world.move(2,0)

            if mp[1]==0:
                world.move(0,-2)
            if mp[1]==479:
                world.move(0,2)


            self.screen.fill([0,0,0])
            world.render(self.screen)

            hovering.render(self.screen, hud, world)

            player.update(world, enemy)

            enemy.play(clock.get_time(), world, player)
            enemy.update(world, player)

            if enemy.gold<enemy.get_smallest_cost()and len(enemy.plants.plants)==0:
                enemy.gold+=25

            if player.gold<player.get_smallest_cost()and len(player.plants.plants)==0:
                self.state="YOU GOLD LOST"
                return

            if len(player.plants.plants)==game_win_plants:
                if level['next_level']:
                    self.last_level=level['next_level']
                    self.state="YOU WON"
                else:
                    self.won_gold=player.gold
                    self.state="YOU WON THE GAME"
                return

            if len(enemy.plants.plants)==game_win_plants:
                self.state="YOU LOST"
                return

            render_all(self.screen, player, enemy, player.locusts+enemy.locusts)

            hud.render(self.screen, player, enemy)
            if hud.do_quit:
                self.state="menu"
                return

            mouse.render(self.screen, hud)

            pygame.display.flip()

def main():
    e=Engine()
    e.main()

if __name__=="__main__":
    main()
