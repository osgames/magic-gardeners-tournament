import pygame
from pygame.locals import*

import os

class Sound:
    def __init__(self,fname,vol=1):
        self.sound = None
        try:
            if pygame.mixer.get_init():
                self.sound = pygame.mixer.Sound(fname)
                self.vol = vol
        except:
            pass
    def play(self):
        if self.sound:
            try:
                if pygame.mixer.get_init():
                    self.sound.set_volume(self.vol)#*Sound.sfx*0.5/100)
                    self.sound.play()
            except:
                pass

def load_image(name, colorkey=None, alpha=None):
    """loads an image into memory"""
    try:
        image = pygame.image.load(name)
    except pygame.error, message:
        print 'Cannot load image:', name
        raise SystemExit, message
    if alpha:
        image=image.convert_alpha()
    else:
        image = image.convert()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0,0))
        image.set_colorkey(colorkey, RLEACCEL)
    return image
