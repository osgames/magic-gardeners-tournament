from core import *

class IsoMap(object):
    def __init__(self, map, tiles, tile_size, tile_mask_image):
        self.map=map

        self.tiles=tiles
        self.tile_size=tile_size
        self.globex=0
        self.globey=0

        self.tile_mask_image=tile_mask_image

        self.tile_locs=[]

    def move(self, x, y):
        self.globex+=x
        self.globey+=y

    def put_tile_at_grid(self,numx,numy):
        half = self.tile_size/2
        quart = self.tile_size/4

        h_ny = half*numy
        
        tilex = numx * half -(h_ny)-self.globex
        tiley = numx * quart + (h_ny - quart)-(quart*numy)-self.globey
        return [tilex, tiley]

    def place_tile(self, screen, kind, rows, cols):
        locs=self.put_tile_at_grid(rows, cols)
        full = self.tile_size
        half = self.tile_size/2
        quart = self.tile_size/4
        h_q = half+quart
        if locs[0]<= screen.get_width() and locs[0] >= -full:
            if locs[1]<= screen.get_height() and locs[1] >= -half:
                self.tile_locs.append([locs, [rows, cols]])
                screen.blit(self.tiles[kind], locs, (0,0,full, h_q))

    def get_tile_at_pos(self, mp):
        point=None
        for i in self.tile_locs:
            if mp[0]>i[0][0] and mp[0] < i[0][0]+self.tile_size:
                if mp[1] > i[0][1] and mp[1] < i[0][1]+self.tile_size-(self.tile_size/2):
                    #we have a hit
                    mp = [mp[0]-i[0][0],mp[1]-i[0][1]]
                    a = self.tile_mask_image.get_at(mp)
                    if a==(0,0,0,255):#pure black
                        point=i[1]
                    if a==(255,0,0,255):#pure red
                        point=[i[1][0],i[1][1]-1]
                    if a==(0,255,0,255):#pure green
                        point=[i[1][0]-1,i[1][1]]
                    if a==(0,0,255,255):#pure blue
                        point=[i[1][0]+1,i[1][1]]
                    if a==(255,255,255,255):#pure white
                        point=[i[1][0],i[1][1]+1]
        if point:
            if point[0]<0 or point[1]<0:
                point=None
            elif point[0]>=len(self.map[0]) or point[1]>=len(self.map):
                point=None
        return point
                

    def render(self, screen):
        self.tile_locs=[]
        for cols in xrange(len(self.map)):
            for rows in xrange(len(self.map[cols])):
                self.place_tile(screen, self.map[cols][rows],
                                rows, cols)

        return screen

class Sprite(object):
    def __init__(self, pos, iso_world, image):
        self.image=image

        self.iso_world=iso_world

        self.pos=pos

        self.rect=self.image.get_rect()
        self.rect.midbottom=self.__get_pos()

    def __get_pos(self):
        p=self.iso_world.put_tile_at_grid(self.pos[0], self.pos[1])
        p[0]+=self.iso_world.tile_size/2
        p[1]+=self.iso_world.tile_size/4
        return p

    def move_tile(self, p):
        self.pos[0]+=p[0]
        self.pos[1]+=p[1]

    def render(self, screen):
        screen.blit(self.image, self.rect)
        return screen

    def update(self, iso_world):
        self.iso_world=iso_world

        if self.pos[0] < 0:
            self.pos[0]=0
        if self.pos[0] > len(self.iso_world.map[0])-1:
            self.pos[0]=len(self.iso_world.map[0])-1

        if self.pos[1] < 0:
            self.pos[1]=0
        if self.pos[1] > len(self.iso_world.map)-1:
            self.pos[1]=len(self.iso_world.map)-1

        self.rect.midbottom=self.__get_pos()

class BottomSprite(object):
    def __init__(self, pos, iso_world, image):
        self.image=image

        self.iso_world=iso_world

        self.pos=pos

        self.rect=self.image.get_rect()
        self.rect.midbottom=self.__get_pos()

    def __get_pos(self):
        p=self.iso_world.put_tile_at_grid(self.pos[0], self.pos[1])
        p[0]+=self.iso_world.tile_size/2
        p[1]+=self.iso_world.tile_size/2
        return p

    def move_tile(self, p):
        self.pos[0]+=p[0]
        self.pos[1]+=p[1]

    def render(self, screen):
        screen.blit(self.image, self.rect)
        return screen

    def update(self, iso_world):
        self.iso_world=iso_world

        if self.pos[0] < 0:
            self.pos[0]=0
        if self.pos[0] > len(self.iso_world.map[0])-1:
            self.pos[0]=len(self.iso_world.map[0])-1

        if self.pos[1] < 0:
            self.pos[1]=0
        if self.pos[1] > len(self.iso_world.map)-1:
            self.pos[1]=len(self.iso_world.map)-1

        self.rect.midbottom=self.__get_pos()
