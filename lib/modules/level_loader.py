
def load(f):
    a=open(f, 'rU').read()
    exec a

    return map_data, player_pos, player_gold,\
           win_num_plants, plants, nourish_cost,\
           nourish_increase, locust_cost,\
           locust_consume_speed, lightning_blast_cost,\
           enemy_start_gold,\
           enemy_pos, enemy_aggresiveness,\
           enemy_cost_multiplier, next_level,\
           enemy_speed, plot_cost, stun_duration,\
           level_start_image

def load_dict(f):
    a=load(f)

    return {'map':a[0],#check
            'player_pos':a[1],#check
            'player_gold':a[2],#check
            'win_num_plants':a[3],
            'plants':a[4],#check
            'nourish_cost':a[5],#check
            'nourish_increase':a[6],#check
            'locust_cost':a[7],#check
            'locust_consume_speed':a[8],#check
            'lightning_bolt_cost':a[9],#check
            'enemy_gold':a[10],#check
            'enemy_pos':a[11],#check
            'enemy_aggresiveness':a[12],#check
            'enemy_cost_multiplier':a[13],#check
            'next_level':a[14],#check
            'enemy_speed':a[15],#check
            'plot_cost':a[16],
            'stun_duration':a[17],
            "level_start_image":a[18]}#check

def load_config(f):
    a=open(f, 'rU').read()
    exec a
    return last_level
