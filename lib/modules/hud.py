import core, gui
from core import *

class Hovering_on(object):
    def __init__(self):
        self.image=core.load_image(os.path.join('data','tiles','selected_tile.png'),
                                   alpha=True)

    def render(self, screen, hud, world):
        pos=pygame.mouse.get_pos()
        if hud.size.collidepoint(pos):
            return

        pos=world.get_tile_at_pos(pos)
        if not pos:
            return
        screen.blit(self.image, world.put_tile_at_grid(pos[0],pos[1]))
        return

def scale(surface, amount):
    surface=pygame.transform.scale(surface, (int(surface.get_width()*amount),
                                             int(surface.get_height()*amount)))
    return surface

class Mouse(object):
    def __init__(self):
        self.images={'select':core.load_image(os.path.join('data','mice','pointer_select.png'),
                                              alpha=True),
                     'buy_plot':core.load_image(os.path.join('data','mice','pointer_bag_of_gold.png'),
                                              alpha=True),
                     'buy_plant':core.load_image(os.path.join('data','mice','pointer_plant.png'),
                                              alpha=True),
                     'harvest':core.load_image(os.path.join('data','mice','pointer_scythe.png'),
                                              alpha=True),
                     'nourish':core.load_image(os.path.join('data','mice','pointer_water_can.png'),
                                              alpha=True),
                     'locust':core.load_image(os.path.join('data','mice','pointer_grasshopper.png'),
                                              alpha=True),
                     'lightning':core.load_image(os.path.join('data','mice','pointer_lightning.png'),
                                              alpha=True)}

        pygame.mouse.set_visible(0)

    def render(self, screen, hud):
        screen.blit(self.images[hud.mouse_mode],pygame.mouse.get_pos())

class GenericMouse(object):
    def __init__(self):
        self.image=core.load_image(os.path.join('data','mice','pointer_select.png'),
                                              alpha=True)
        pygame.mouse.set_visible(0)

    def render(self, screen):
        screen.blit(self.image,pygame.mouse.get_pos())

class PlantSelectionWizard(object):
    def __init__(self, pos,
                 font=os.path.join('data','assets','Onciale.ttf'),
                 font_size=20,
                 font_color=[255,255,255,255],
                 font_hover_color=[255,0,0,255],
                 click_button=1):
        self.num_buttons=0
        self.pos=pos
        self.messages=[]
        self.click_funcs=[]
        self.font=font
        self.font_size=font_size
        self.font_color=font_color
        self.font_hover_color=font_hover_color
        self.click_button=click_button

    def add_button(self, name, message, func):
        self.messages.append("%s: %s"%(name,message))
        self.click_funcs.append(func)
        self.num_buttons+=1

    def comp(self):
        return gui.TextOnlyButtonList(self.num_buttons,
                              self.pos,
                              self.messages,
                              self.click_funcs,
                              self.font,
                              self.font_size,
                              self.font_color,
                              self.font_hover_color,
                              self.click_button)
                              

class HUD(object):
    def __init__(self, player):
        self.font=pygame.font.Font(os.path.join('data','assets','Onciale.ttf'), 24)

        self.mouse_mode="select"
        self.mouse_modes=["select","buy_plot","buy_plant", "nourish",
                          "locust", "lightning", "harvest"]

        self.size=pygame.rect.Rect((472,0),(125,480))
        self.fixed_size=pygame.rect.Rect((515,0),(125,480))
        font_path=os.path.join('data','assets','Onciale.ttf')

        self.gold_text_pos=[520,10]
        self.plots_text_pos=[375,5]
        self.e_plots_text_pos=[375,40]
        self.buttons=[gui.Button([537,75],
                                 core.load_image(os.path.join('data','mice','reg_bag_of_gold.png'),
                                                 alpha=True),
                                 "%s"%player.plot_cost, self.reactBuy_Plot,
                                 font_hover_color=[255,0,0,255],
                                 font=font_path, font_size=24),
                      gui.Button([605,133],
                                 core.load_image(os.path.join('data','mice','reg_plant.png'),
                                                 alpha=True),
                                 "", self.reactBuy_Plant,
                                 font_hover_color=[255,0,0,255],
                                 font=font_path, font_size=24),
                      gui.Button([542,195],
                                 core.load_image(os.path.join('data','mice','reg_scythe.png'),
                                                 alpha=True),
                                 "", self.reactHarvest,
                                 font_hover_color=[255,0,0,255],
                                 font=font_path, font_size=22),
                      gui.Button([604,265],
                                 core.load_image(os.path.join('data','mice','reg_water_can.png'),
                                                 alpha=True),
                                 "%s"%player.nourish_cost, self.reactNourish,
                                 font_hover_color=[255,0,0,255],
                                 font=font_path, font_size=22),
                      gui.Button([541,330],
                                 core.load_image(os.path.join('data','mice','reg_grasshopper.png'),
                                                 alpha=True),
                                 "%s"%player.locust_cost, self.reactLocust,
                                 font_hover_color=[255,0,0,255],
                                 font=font_path, font_size=24),
                      gui.Button([604,385],
                                 core.load_image(os.path.join('data','mice','reg_lightning.png'),
                                                 alpha=True),
                                 "%s"%player.lightning_bolt_cost, self.reactLightning,
                                 font_hover_color=[255,0,0,255],
                                 font=font_path, font_size=20),
                      gui.TextOnlyButton([575,460],
                                 "Quit", self.reactQuit,
                                 font_hover_color=[255,255,0,255],
                                 font=font_path, font_size=25)]

        for i in self.buttons:
            i.rect=i.rect.inflate(-3,-3)

        self.plant_types=PlantSelectionWizard([515, 135])
        for i in player.plant_types:
            self.plant_types.add_button(i, player.plant_types[i][1],
                                        self.reactPlantList)

        self.plant_types=self.plant_types.comp()

        self.plant_list_on=False
        gotcha=False
        for i in player.plant_types:
            if not gotcha:
                self.default_plant=i
                gotcha=True
        self.selected_plant=self.default_plant

        self.bg_image=core.load_image(os.path.join('data','assets','game_hud.png'),
                                      alpha=True)

        self.do_quit=False

    def reactPlantList(self):
        for i in self.plant_types.buttons:
            if i.hovering:
                self.selected_plant=i.message.split(':')[0]

    def reactBuy_Plot(self):
        self.mouse_mode="buy_plot"

    def reactBuy_Plant(self):
        self.mouse_mode="buy_plant"
        self.plant_list_on=True

    def reactNourish(self):
        self.mouse_mode="nourish"

    def reactLocust(self):
        self.mouse_mode="locust"

    def reactLightning(self):
        self.mouse_mode="lightning"

    def reactHarvest(self):
        self.mouse_mode="harvest"

    def reactQuit(self):
        self.do_quit=True

    def render(self, screen, player, enemy):
        screen.blit(self.bg_image, self.size)
        screen.blit(self.font.render('gold: %s'%player.gold,
                                     True, (255,255,255,255)),
                    self.gold_text_pos)
        screen.blit(self.font.render('plants: %s/%s'%(len(player.plants.plants),
                                                     player.win_num_plants),
                                     True, (0,0,255,255)),
                    self.plots_text_pos)
        screen.blit(self.font.render('plants: %s/%s'%(len(enemy.plants.plants),
                                                     enemy.win_num_plants),
                                     True, (255,0,0,255)),
                    self.e_plots_text_pos)

        for i in self.buttons:
            i.render(screen)
        if self.plant_list_on:
            self.plant_types.render(screen)

    def event(self,e):
        was_clicked=bool(self.plant_list_on)
        if self.plant_list_on:
            self.plant_types.update(e)
        else:
            for i in self.buttons:
                i.update(e)

        if was_clicked:
            if e.type==MOUSEBUTTONDOWN:
                if e.button==1 or e.button==3:
                    self.plant_list_on=False

