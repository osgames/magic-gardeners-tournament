from core import *

class Button(object):
    def __init__(self, pos, image, message, click_func,
                 font=None, font_size=32, font_color=[255,255,255,255],
                 font_hover_color=[255,255,255,255],click_button=1):

        self.reg_image=image
        self.rect=image.get_rect()
        self.rect.center=tuple(pos)
        self.hover_image=pygame.transform.scale(image.copy(), (int(self.rect.width*0.85),
                                                               int(self.rect.height*0.85)))
        self.hi_rect=self.hover_image.get_rect()
        self.hi_rect.center=self.rect.center

        self.font=pygame.font.Font(font, font_size)
        self.small_font=pygame.font.Font(font, int(font_size*0.85))
        self.font_color=font_color
        self.font_hover_color=font_hover_color

        self.message=message

        self.hovering=False
        self.am_clicked=False

        self.click_button=click_button
        self.click_func=click_func

    def update(self, event):
        if event.type==MOUSEBUTTONDOWN:
            if event.button==self.click_button:
                if self.rect.collidepoint(pygame.mouse.get_pos()):
                    self.click_func()
                    self.am_clicked=True
                else:
                    self.am_clicked=False
                    pass
        if event.type==MOUSEBUTTONUP:
            if event.button==self.click_button:
                self.am_clicked=False
        if self.rect.collidepoint(pygame.mouse.get_pos()):
            self.hovering=True
        else:
            self.hovering=False

    def render(self, screen):
        if self.hovering:
            screen.blit(self.hover_image, self.hi_rect)
            a=self.small_font.render(self.message, True, self.font_hover_color)
            r=a.get_rect()
            r.center=self.hi_rect.center
            screen.blit(a,r)
        else:
            screen.blit(self.reg_image, self.rect)
            a=self.font.render(self.message, True, self.font_color)
            r=a.get_rect()
            r.center=self.rect.center
            screen.blit(a,r)
        return screen

class TextOnlyButton(object):
    def __init__(self, pos, message, click_func,
                 font=None, font_size=32, font_color=[255,255,255,255],
                 font_hover_color=[255,255,255,255],click_button=1):

        self.font=pygame.font.Font(font, font_size)
        self.small_font=pygame.font.Font(font, int(font_size*0.85))
        self.font_color=font_color
        self.font_hover_color=font_hover_color

        self.rect=self.font.render(message, True, (255,255,255,255)).get_rect()
        self.hi_rect=self.small_font.render(message,True,(255,255,255,255)).get_rect()
        self.rect.center=pos
        self.hi_rect.center=pos

        self.message=message

        self.hovering=False
        self.am_clicked=False

        self.click_button=click_button
        self.click_func=click_func

    def update(self, event):
        if event.type==MOUSEBUTTONDOWN:
            if event.button==self.click_button:
                if self.rect.collidepoint(pygame.mouse.get_pos()):
                    self.click_func()
                    self.am_clicked=True
                else:
                    self.am_clicked=False
                    pass
        if event.type==MOUSEBUTTONUP:
            if event.button==self.click_button:
                self.am_clicked=False
        if self.rect.collidepoint(pygame.mouse.get_pos()):
            self.hovering=True
        else:
            self.hovering=False

    def render(self, screen):
        if self.hovering:
            a=self.small_font.render(self.message, True, self.font_hover_color)
            r=a.get_rect()
            r.center=self.hi_rect.center
            screen.blit(a,r)
        else:
            a=self.font.render(self.message, True, self.font_color)
            r=a.get_rect()
            r.center=self.rect.center
            screen.blit(a,r)
        return screen

class ButtonList(object):
    def __init__(self, num_buttons, pos, image, messages, click_funcs,
                 font=None, font_size=32, font_color=[255,255,255,255],
                 font_hover_color=[255,255,255,255],click_button=1):

        self.pos=pos
        self.buttons=[]
        shift_down=image.get_height()*1.25
        for i in range(num_buttons):
            self.buttons.append(Button([self.pos[0],self.pos[1]+shift_down*i],
                                       image, messages[i], click_funcs[i],
                                       font, font_size, font_color,
                                       font_hover_color, click_button))

    def update(self, event):
        for i in self.buttons:
            i.update(event)

    def render(self, screen):
        for i in self.buttons:
            i.render(screen)

class TextOnlyButtonList(object):
    def __init__(self, num_buttons, pos, messages, click_funcs,
                 font=None, font_size=32, font_color=[255,255,255,255],
                 font_hover_color=[255,255,255,255],click_button=1):

        self.pos=pos
        self.buttons=[]
        f=pygame.font.Font(font, font_size)
        r=f.render(messages[0],True,(255,255,255,255)).get_rect()
        shift_down=r.height*1.25
        for i in range(num_buttons):
            self.buttons.append(TextOnlyButton([self.pos[0],self.pos[1]+shift_down*i],
                                       messages[i], click_funcs[i],
                                       font, font_size, font_color,
                                       font_hover_color, click_button))

    def update(self, event):
        for i in self.buttons:
            i.update(event)

    def render(self, screen):
        for i in self.buttons:
            i.render(screen)
