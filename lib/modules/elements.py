import core, isometric, entities, gui
from core import *

import random

def random_dict(d):
    a=[]
    for i in d:
        a.append(i)
    a=random.choice(a)
    return a

def repr_list(l,a):
    nl=[]
    for i in range(a):
        nl=nl+l
    return nl

def random_pos(player, enemy):
    l=[player.entity]+player.plants.plants+\
       repr_list(player.locusts,5)+repr_list(enemy.locusts,2)
    a=random.choice(l)
    return a.pos

class locustSwarm(object):
    def __init__(self, pos, iso_world, speed):
        self.entity=entities.AnimatedBottomSprite(pos,
                            iso_world,
                            [core.load_image(os.path.join('data','objects','locusts1.png'),
                                             alpha=True),
                             core.load_image(os.path.join('data','objects','locusts2.png'),
                                             alpha=True),
                             core.load_image(os.path.join('data','objects','locusts3.png'),
                                             alpha=True),
                             core.load_image(os.path.join('data','objects','locusts4.png'),
                                             alpha=True),
                             core.load_image(os.path.join('data','objects','locusts5.png'),
                                             alpha=True),
                             core.load_image(os.path.join('data','objects','locusts6.png'),
                                             alpha=True)],
                            12)

        self.speed=speed
        self.timer=0
        self.just_moved=False
        self.previous_pos=[0,0]
        self.just_moved_counter=0

    def choose_nearest(self, player, enemy):
        try:
            a=player.plants.plant_locs+enemy.plants.plant_locs
            for i in player.locusts+enemy.locusts:
                if i.entity.pos in a:
                    a.remove(i.entity.pos)
            return random.choice(a)
        except:
            return self.entity.pos

    def update(self, world, player, enemy):
        if self.entity.pos in player.plants.plant_locs+enemy.plants.plant_locs:
            self.just_moved_counter+=1
            if self.just_moved_counter>=150:
                self.just_moved=False
                self.just_moved_counter=0
            self.timer+=1
            if self.timer>=self.speed:
                self.timer=0
                for i in player.plants.plants:
                    if self.entity.pos == i.pos:
                        player.plants.plant_locs.remove(i.pos)
                        player.plants.plants.remove(i)
                for i in enemy.plants.plants:
                    if self.entity.pos == i.pos:
                        enemy.plants.plant_locs.remove(i.pos)
                        enemy.plants.plants.remove(i)
        else:
            self.timer=0
            self.just_moved=True
            self.previous_pos=list(self.entity.pos)
            self.entity.pos=self.choose_nearest(player, enemy)
        self.entity.update(world)
        self.entity.animate()
                    

class Player(object):
    def __init__(self, iso_world, level):
        self.entity=entities.Player(level['player_pos'], iso_world,
                                    core.load_image(os.path.join('data','objects',
                                                                 'wizard.png'),
                                                    alpha=True))

        self.plots=entities.STHolder()
        self.plants=entities.PlantHolder()
        self.nourishes=entities.NourishHolder()
        self.locusts=[]

        self.do_sound=True
        self.sounds={'locust':core.Sound(os.path.join('data','sfx',
                                                      'locust.wav')),
                     'lightning':core.Sound(os.path.join('data','sfx',
                                                         'lightning.wav'),1.5),
                     'harvest':core.Sound(os.path.join('data','sfx',
                                                         'coin.wav')),
                     'nourish':core.Sound(os.path.join('data','sfx',
                                                         'water.wav'))}

        self.stunned_image=entities.AnimatedBottomSprite(self.entity.pos,
                                    iso_world,
                                    [core.load_image(os.path.join('data',
                                                                  'objects',
                                                                  'wizard_stunned1.png'),
                                                                  alpha=True),
                                     core.load_image(os.path.join('data',
                                                                  'objects',
                                                                  'wizard_stunned2.png'),
                                                                  alpha=True),
                                     core.load_image(os.path.join('data',
                                                                  'objects',
                                                                  'wizard_stunned3.png'),
                                                                  alpha=True),
                                     core.load_image(os.path.join('data',
                                                                  'objects',
                                                                  'wizard_stunned4.png'),
                                                                  alpha=True)],
                                                         15)

        self.gold=level['player_gold']
        self.plot_cost=level['plot_cost']

        self.nourish_increase=level['nourish_increase']
        self.nourish_cost=level['nourish_cost']

        self.locust_cost=level['locust_cost']
        self.locust_consume_speed=level['locust_consume_speed']

        self.lightning_bolt_cost=level['lightning_bolt_cost']
        self.stun_duration=level['stun_duration']

        self.win_num_plants=level['win_num_plants']

        self.stunned=False
        self.stun_counter=0

        self.plant_types=level['plants']
        self.world=iso_world

    def get_smallest_cost(self):
        l=[self.plot_cost, self.nourish_cost, self.locust_cost,
           self.lightning_bolt_cost]
        for i in self.plant_types:
            l.append(self.plant_types[i][1])
        return min(l)

    def update(self, world, other):
        if len(self.plants.plants+other.plants.plants)==0:
            self.locusts=[]
            other.locusts=[]
        self.world=world
        self.entity.update(world)
        for i in self.plants.plants:
            if not self.stunned:i.grow()
            i.update(world)
            if i.dead:
                self.plants.plant_locs.remove(i.pos)
                self.plants.plants.remove(i)
        if self.stunned:
            self.stun_counter+=1
            if self.stun_counter>=self.stun_duration:
                self.stun_counter=0
                self.stunned=False
            self.stunned_image.animate()
        self.stunned_image.update(world)
        self.plots.update(world, self.plants)
        self.nourishes.update(world, self.plants)
        for i in self.locusts:
            i.update(world, self, other)

    def buy_plant(self, pos, world, t='base'):
        if self.gold>=self.plant_types[t][1]:
            if pos==None:
                return
            if self.plots.can_go(pos):
                if not self.plants.get_filled(pos):
                    self.gold-=self.plant_types[t][1]
                    self.plants.add_plant(entities.Plant(pos,world,self.plant_types[t][0],
                                                         self.plant_types[t][2],
                                                         self.plant_types[t][3],
                                                         reward=self.plant_types[t][4]))
                    return self.plants

    def harvest(self, pos):
        if self.do_sound:
            self.sounds['harvest'].play()
        if pos in self.plants.plant_locs:
            self.plants.plant_locs.remove(pos)
            for i in self.plants.plants:
                if i.pos==pos:
                    if i.mature:
                        self.gold+=i.reward
                    else:
                        a=int(i.reward*(i.age/200))
                        if a<0:
                            a=0
                        self.gold+=a
                    self.plants.plants.remove(i)

    def nourish(self, pos, world):
        if self.do_sound:
            self.sounds['nourish'].play()
        for i in self.plants.plants:
            if self.gold>=self.nourish_cost:
                if i.pos==pos:
                    self.gold-=self.nourish_cost
                    plot = entities.NourishedTile(pos, world,
                                                  core.load_image(os.path.join('data','tiles',
                                                                               'nourished_plot.png'),
                                                                  alpha=True))
                    self.nourishes.add_tile(plot)
                    for i in self.plants.plants:
                        if i.pos==plot.pos:
                            i.growth_speed*=self.nourish_increase

    def cast_lightning_bolt(self, pos, world, enemy):
        if self.do_sound:
            self.sounds['lightning'].play()
        if self.gold>=self.lightning_bolt_cost:
            self.gold-=self.lightning_bolt_cost
            enemy.react_lightning_attack(pos)
            for i in self.locusts:
                if i.entity.pos==pos:
                    self.locusts.remove(i)
            for i in enemy.locusts:
                if i.entity.pos==pos:
                    enemy.locusts.remove(i)

    def react_lightning_attack(self, pos):
        for i in self.plants.plants:
            if i.pos==pos:
                self.plants.plant_locs.remove(i.pos)
                self.plants.plants.remove(i)

        for i in self.nourishes.selectedTiles:
            if i.pos==pos:
                self.nourishes.STs.remove(i.pos)
                self.nourishes.selectedTiles.remove(i)
        for i in self.locusts:
            if i.entity.pos==pos:
                self.locusts.remove(i)

        if self.entity.pos==pos:
            self.stunned=True

    def cast_locusts(self, pos, world):
        if self.do_sound:
            self.sounds['locust'].play()
        if self.gold>=self.locust_cost:
            self.gold-=self.locust_cost
            self.locusts.append(locustSwarm(pos, world, self.locust_consume_speed))

    def buy_plot(self, pos, world):
        if self.do_sound:
            self.sounds['harvest'].play()
        if not pos in self.plots.STs:
            if self.gold>=self.plot_cost:
                self.gold-=self.plot_cost
                plot=entities.SelectedTile(pos,world,
                  [core.load_image(os.path.join('data','tiles','owned_plot.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot1.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot2.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot3.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot4.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot3.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot2.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot1.png'),
                                 alpha=True)],
                  [core.load_image(os.path.join('data','tiles','owned_plot_mat.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot1_mat.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot2_mat.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot3_mat.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot4_mat.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot3_mat.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot2_mat.png'),
                                 alpha=True),
                   core.load_image(os.path.join('data','tiles','owned_plot1_mat.png'),
                                 alpha=True)],
                  20)

                self.plots.add_tile(plot)

    def render(self, screen):
        self.plots.render(screen)
        self.entity.render(screen)
        self.plants.render(screen)
        self.nourishes.render(screen)

class Enemy(Player):
    def __init__(self, iso_world, level):
        Player.__init__(self, iso_world, level)

        self.cur_time=0.0
        self.speed=level['enemy_speed']

        self.entity.pos=level['enemy_pos']
        self.stunned_image.pos=self.entity.pos
        self.entity.image=core.load_image(os.path.join('data','objects',
                                                       'wizard_bad.png'),
                                          alpha=True)

        self.gold=level['enemy_gold']

        self.aggressiveness=level['enemy_aggresiveness']
        self.agg_counter=0
        self.cost_multiplier=level['enemy_cost_multiplier']

        self.nourish_cost*=self.cost_multiplier

        self.locust_cost*=self.cost_multiplier

        self.lightning_bolt_cost*=self.cost_multiplier

        self.reg_images=[core.load_image(os.path.join('data','tiles','enemy_plot.png'),
                                          alpha=True)]

    def get_random_plant_within_budget(self):
        a=random_dict(self.plant_types)
        if not self.plant_types[a][1]*self.cost_multiplier>self.gold:
            return a
        for i in self.plant_types:
            return i

    def nourish(self, pos, world):
        for i in self.plants.plants:
            if i.pos==pos:
                plot = entities.NourishedTile(pos, world,
                                              core.load_image(os.path.join('data','tiles',
                                                                           'enemy_nourished_plot.png'),
                                                              alpha=True))
                self.nourishes.add_tile(plot)
                for i in self.plants.plants:
                    if i.pos==plot.pos:
                        i.growth_speed*=self.nourish_increase

    def go_now(self, world, player):
        if len(self.plots.STs)==0:
            pos=[random.randint(0,len(world.map[0])-1),
                 random.randint(0,len(world.map)-1)]
            if not pos in [player.entity.pos, self.entity.pos]+player.plots.STs:
                self.buy_plot(pos, world)

        if len(self.plants.plants)<len(self.plots.STs):
            empties=[]
            for i in self.plots.selectedTiles:
                if not i.have_plant:
                    empties.append(i.pos)
            try:
                pos=random.choice(empties)
                if not pos in [player.entity.pos, self.entity.pos]+player.plots.STs:
                    self.buy_plant(pos, world,self.get_random_plant_within_budget())
            except:
                pass

        if random.randint(2,8)==5:
            pos=[random.randint(0,len(world.map[0])-1),
                 random.randint(0,len(world.map)-1)]
            if not pos in [player.entity.pos, self.entity.pos]+player.plots.STs:
                self.buy_plot(pos, world)

        if random.randint(1,6)==5:
            if len(self.plants.plants)>0:
                pos=random.choice(self.plants.plant_locs)
                if not pos in [player.entity.pos, self.entity.pos]+player.nourishes.STs:
                    self.nourish(pos, world)

        self.agg_counter+=1
        if len(self.plants.plants)>=1:
            if self.gold>=self.lightning_bolt_cost:
                if random.choice([True,False]):
                    if self.agg_counter>=self.aggressiveness:
                        a=random_pos(player, self)
                        self.cast_lightning_bolt(a, world, player)
                        player.react_lightning_attack(a)
                        self.gold-=self.lightning_bolt_cost
        locs=player.locusts+self.locusts
        for i in locs:
            if i.entity.pos in self.plants.plant_locs:
                if self.gold>=self.lightning_bolt_cost:
                    self.gold-=self.lightning_bolt_cost
                    self.cast_lightning_bolt(i.entity.pos, world, player)
                    player.react_lightning_attack(i.entity.pos)

        if self.agg_counter>=self.aggressiveness:
            if len(self.plants.plants)>=1:
                if self.gold>=self.locust_cost:
                    if random.choice([False,True]):
                        self.gold-=self.locust_cost
                        pos=[random.randint(0,len(world.map[0])-1),
                             random.randint(0,len(world.map)-1)]
                        self.cast_locusts(pos, world)
        if self.agg_counter>=self.aggressiveness:
            self.agg_counter=0

        for i in self.plants.plants:
            if i.mature:
                self.harvest(i.pos)

    def play(self, clock_speed, world, player):
        for i in self.plots.selectedTiles:
            i.reg_images=self.reg_images
            i.mat_images=self.reg_images
            i.images=self.reg_images
        if not self.stunned:
            self.cur_time+=float(clock_speed)/1000
            if self.cur_time>=self.speed:
                self.go_now(world, player)
                self.cur_time=0
        else:
            self.stun_counter+=1
            if self.stun_counter>=self.stun_duration:
                self.stun_counter=0
                self.stunned=False

class OBJ(object):
    def __init__(self, ent, priority):
        self.ent=ent
        self.priority=priority

class OBJ_list(object):
    def __init__(self):
        self.objs=[]

    def my_sort(self, x, y):
            if x.ent.pos[1]<y.ent.pos[1]:
                return -1
            elif x.ent.pos[1]>y.ent.pos[1]:
                return 1

            if x.ent.pos[0]<y.ent.pos[0]:
                return -1
            elif x.ent.pos[0]>y.ent.pos[0]:
                return 1
            if x.priority>y.priority:
                return -1
            elif x.priority<y.priority:
                return 1
            return 0

    def add_obj(self, obj, priority):

        new_obj=OBJ(obj, priority)
        self.objs.append(new_obj)

    def comp_list(self):
        self.objs.sort(self.my_sort)

    def render(self, screen):
        for i in self.objs:
            i.ent.render(screen)
            
def render_all(screen, player, enemy, locusts):

    obj=OBJ_list()
    for i in player.plots.selectedTiles:
        obj.add_obj(i, 10)

    for i in enemy.plots.selectedTiles:
        obj.add_obj(i, 10)

    for i in player.nourishes.selectedTiles:
        obj.add_obj(i, 8)

    for i in enemy.nourishes.selectedTiles:
        obj.add_obj(i, 8)

    for i in player.plants.plants:
        obj.add_obj(i, 9)

    for i in enemy.plants.plants:
        obj.add_obj(i, 9)

    obj.add_obj(player.entity, 7)

    obj.add_obj(enemy.entity, 7)

    for i in locusts:
        obj.add_obj(i.entity, 6)

    if player.stunned:
        obj.add_obj(player.stunned_image, 6)

    if enemy.stunned:
        obj.add_obj(enemy.stunned_image, 6)

    obj.comp_list()
    obj.render(screen)
    for i in locusts:
        if i.just_moved:
            minx=i.previous_pos[0]
            miny=i.previous_pos[1]
            maxx=i.entity.pos[0]
            maxy=i.entity.pos[1]
            minx,miny=player.world.put_tile_at_grid(minx, miny)
            maxx,maxy=player.world.put_tile_at_grid(maxx, maxy)
            minx+=50
            maxx+=50
            miny+=25
            maxy+=25
            pygame.draw.line(screen, (255,125,0,255), (minx, miny), (maxx, maxy), 5)
