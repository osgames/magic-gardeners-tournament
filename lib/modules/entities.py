from core import *
import isometric

class AnimatedSprite(isometric.Sprite):
    def __init__(self, pos, iso_world, images, switch_rate):
        isometric.Sprite.__init__(self, pos, iso_world, images[0])
        self.switch_rate=switch_rate
        self.cur_pos=0
        self.timer=0

        self.images=images

    def animate(self):
        self.timer+=1
        if self.timer>=self.switch_rate:
            self.timer=0
            self.cur_pos+=1
        if self.cur_pos>=len(self.images):
            self.cur_pos=0
        self.image=self.images[self.cur_pos]

class AnimatedBottomSprite(isometric.BottomSprite):
    def __init__(self, pos, iso_world, images, switch_rate):
        isometric.BottomSprite.__init__(self, pos, iso_world, images[0])
        self.switch_rate=switch_rate
        self.cur_pos=0
        self.timer=0

        self.images=images

    def animate(self):
        self.timer+=1
        if self.timer>=self.switch_rate:
            self.timer=0
            self.cur_pos+=1
        if self.cur_pos>=len(self.images):
            self.cur_pos=0
        self.image=self.images[self.cur_pos]

class Player(isometric.BottomSprite):
    def __init__(self, pos, iso_world, image):
        isometric.BottomSprite.__init__(self,pos,iso_world,image)

class Plant(isometric.Sprite):
    def __init__(self, pos, iso_world, image,
                 growth_speed,stay_mature_for,
                 reward=100):
        isometric.Sprite.__init__(self,pos,iso_world,image)

        self.age=0
        self.mature=False

        self.mature_counter=0
        self.stay_mature_for=stay_mature_for

        self.growth_speed=growth_speed

        self.base_image=image

        self.reward=reward

        self.max_height=self.image.get_height()
        self.max_width=self.image.get_width()
        self.dead=False

    def grow(self):
        if self.age>=100:
            self.mature=True
            self.mature_counter+=1
            if self.mature_counter>=self.stay_mature_for:
                self.age=1
                self.mature=False
                self.mature_counter=0
                self.dead=True
        self.age+=self.growth_speed

        if not self.mature:
            sizex=int(self.max_width/(100/self.age))
            sizey=int(self.max_height/(100/self.age))
        else:
            sizex=self.max_width
            sizey=self.max_height
        self.image=pygame.transform.scale(self.base_image, [sizex,sizey])
        self.rect=self.image.get_rect()

class PlantHolder(object):
    def __init__(self):
        self.plants=[]
        self.plant_locs=[]

    def get_filled(self, pos):
        if pos in self.plant_locs:
            return True
        return False

    def add_plant(self, plant):
        self.plants.append(plant)
        self.plant_locs.append(plant.pos)

        def my_sort(x, y):
            if x.pos[1]<y.pos[1]:
                return -1
            if x.pos[1]>y.pos[1]:
                return 1
            if x.pos[0]<y.pos[0]:
                return -1
            if x.pos[0]>y.pos[0]:
                return 1
            return 0

        self.plants.sort(my_sort)

    def render(self, screen):
        for i in self.plants:
            i.render(screen)

class SelectedTile(AnimatedBottomSprite):
    def __init__(self, pos, world, reg_images,
                 mat_images, switch_rate):
        AnimatedBottomSprite.__init__(self, pos, world, reg_images, switch_rate)
        self.reg_images=reg_images
        self.mat_images=mat_images
        self.have_plant=False

    def check_mature(self, plants):
        for i in plants.plants:
            if i.pos == self.pos:
                self.have_plant=True
                if i.mature:
                    self.images=self.mat_images
                else:
                    self.images=self.reg_images
                return
        self.have_plant=False
        self.images=self.reg_images
        self.timer=0
        self.cur_pos=0

class STHolder(object):
    def __init__(self):
        self.selectedTiles=[]
        self.STs=[]

    def add_tile(self, tile):
        if not tile.pos in self.STs:
            self.selectedTiles.append(tile)
            def my_sort(x, y):
                if x.pos[1]<y.pos[1]:
                    return -1
                if x.pos[1]>y.pos[1]:
                    return 1
                if x.pos[0]<y.pos[0]:
                    return -1
                if x.pos[0]>y.pos[0]:
                    return 1
                return 0
            self.selectedTiles.sort(my_sort)
            self.STs.append(tile.pos)

    def can_go(self,pos):
        if pos in self.STs:
            return True
        return False
    def render(self, screen):
        for i in self.selectedTiles:
            i.render(screen)

    def update(self, world, plants):
        for i in self.selectedTiles:
            i.update(world)
            i.animate()
            i.check_mature(plants)


class NourishedTile(isometric.BottomSprite):
    def __init__(self, pos, world, image):
        isometric.BottomSprite.__init__(self, pos, world, image)

class NourishHolder(object):
    def __init__(self):
        self.selectedTiles=[]
        self.STs=[]

    def add_tile(self, tile):
        if not tile.pos in self.STs:
            self.selectedTiles.append(tile)
            def my_sort(x, y):
                if x.pos[1]<y.pos[1]:
                    return -1
                if x.pos[1]>y.pos[1]:
                    return 1
                if x.pos[0]<y.pos[0]:
                    return -1
                if x.pos[0]>y.pos[0]:
                    return 1
                return 0
            self.selectedTiles.sort(my_sort)
            self.STs.append(tile.pos)

    def render(self, screen):
        for i in self.selectedTiles:
            i.render(screen)

    def update(self, world, plants):
        for i in self.selectedTiles:
            i.update(world)
            got=False
            for x in plants.plants:
                if x.pos==i.pos:
                    got=True
            if not got:
                self.STs.remove(i.pos)
                self.selectedTiles.remove(i)
