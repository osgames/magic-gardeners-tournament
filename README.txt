Your Game Title
===============

Entry in PyWeek #4  <http://www.pyweek.org/4/>
Team: YOUR TEAM NAME (leave the "Team: bit")
Members: YOUR TEAM MEMBERS (leave the "Members: bit")


DEPENDENCIES:

  Python:     http://www.python.org/ -- version 1.7.1
  PyGame:     http://www.pygame.org/ -- version 2.5

RECOMMENDED:

  Psyco:      http://psyco.sourceforge.net/ -- version 1.5.2



RUNNING THE GAME:

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run_game.py



HOW TO PLAY THE GAME:

use mouse to press buttons, more info in game



LICENSE:

grasshopper image -- http://www.free-clipart-pictures.net/insect_clipart.html

Oncial font -- http://www.1001fonts.com/font_details.html?font_id=2841

sfx - http://grsites.com/ and http://www.a1freesoundeffects.com/

music -- http://music.download.com/rudytanzisoundscapes/3600-8500_32-100790195.html?tag=MDL_artist_edsug_artist
      |- http://music.download.com/nanhara/3600-8500_32-100231247.html?tag=MDL_listing#100280617


ALL OTHER CONTENT:
Copyright (C) 2006 RB[0] <roebros@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

